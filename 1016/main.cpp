#include <iostream>
#include <math.h>
using namespace std;

#define min(a, b)   (a > b ? b : a)

char b[20] = {0};
int c[20] = {0};

//Return 1:is prime 0 :not prime
char isPrime(int num)
{
    int i = 0;
    int tmp =sqrt( num);
    for(i= 2;i <=tmp; i++)
    if(num %i== 0)
      return 0 ;

    return 1 ;
}

int prime(int n, int pos)
{
    int i = 0;

    if(pos == n)
    {
        if(isPrime(c[pos-1] + 1))
        {
            int p = 0;
            for(p=0; p < n; p++)
	    {
                cout <<c[p];
		if(p != n-1)
			cout << " ";
            }
            cout << endl;
        }
        return 0;
    }

    for(i=2; i <= n; i++)
    {
        if(b[i] == 0)
        {
            if(isPrime(i + c[pos -1]))
            {
                c[pos] = i;
                b[i] = 1;
                pos += 1;


                if(prime(n, pos) == -1)
                {
                    b[i] = 0;
                    pos -= 1;
                }else{
                    b[i] = 0;
                    pos -= 1;
                }
            }
        }
    }

    return -1;
}

int main()
{
    int k = 1;
    int n = 0;
    while(cin >> n)
    {
	if(k >1)
		cout << endl;
        cout <<"Case "<<k<<":"<<endl;
        c[0] = 1;
        b[1] = 1;
        prime(n, 1);
        k++;
    }
    return 0;
}
