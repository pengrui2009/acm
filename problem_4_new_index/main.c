/*
 * 功能说明：有一组自然数，它们分别为1,2,3,5,6,7,8,9,10,11,12,13,15...(自然数中不能包含4这个字符)
 * 然后输入N，输出位置N出的数据的值
 */
#include <stdio.h>
#include <math.h>
//设计思路:
/*
 *首先这里有一个规律：
 * 10：9 * 0 + 1 (包含字符4的数字个数)
 * 100：9 * (9 * 0 + 1) + 10
 * 1000: 9 * (9 * (9 * (9 * 0 + 1) + 10)) + 100
 * 假设f(n)为自然数1-n中包含字符4的数据的个数，则有如下关系：f(n) = f(n-1) + power(10, n-1) ,此处的n指能被10整除的数;
 * 如果n不能被10整除，则有如下规律：
 *  假设bit[m]代表n的个十百千...位上的值
 *  则有如下公式
    f(n) = (bit[m] > 4 ? bit[m] : (bit[m] - 1)) * f(n - 1) + power(10, n-1) + (bit[m - 1] > 4 ? bit[m-1] : (bit[m-1] - 1))
        * f(n-2) + power(10 ,n-2) + ...
 *  现在假设data[index]为我们index索引输出值
    那么data[index]必然在[index, 5 * index]之间，这样的话，我们可以使用二分查找的方法，并结合上述的公式计算出所需的值
 */


/*
 * 函数功能：计算数据1到data数据之间含有不包括字符4的数据的个数
 * 返回：1到data数据之间不包括字符4的数据的个数
 */
int cal_fourth(int data)
{
    int sum = 0;
    int i = 0, j = 0;
    int index = 0;
    int val = data;
    int mod = 0;
    //将数据data的每一位通过除余算法放入data_bit中,0为个位，...
    int data_bit[15] = {0};
    //每一位上包含字符4数字的个数，如 78, data_count[0] = 1, data_count[1] = 6 * 1 + 10
    int data_count[15] = {0};

    //计算data的每一bit位值
    while(val > 0)
    {
        mod = val % 10;
        val = val / 10;
        data_bit[index++] = mod;
    }

    //计算1~data数据中的每一bit位上包含能字符4个数
    for(i=0; i<index; i++)
    {
        if(data_bit[i] > 4)
        {
            data_count[i] = (data_bit[i] - 1) * data_count[i] + pow(10, i);
        }else{
            data_count[i] = (data_bit[i]) * data_count[i];
        }

        for(j=i+1; j<index; j++)
                data_count[j] = 9 * data_count[j] + pow(10, i);

    }

    //计算1~data所有包含字符4的数据的总个数
    for(i=0; i<index; i++)
        sum += data_count[i];

    //返回1~data所有不包含字符4的数据的总个数
    return (data - sum);
}

/*
 *函数功能：判断一个数是否包含字符4
 *返回：1：包含  0：不包含
 */
int is_containof_fourth(int n)
{
    int val = n;
    int mod = 0;
    while(val >= 10)
    {
        mod = val % 10;
        val = val / 10;

        if(mod == 4)
            return 1;
    }

    if(val == 4)
        return 1;

    return 0;
}

/*
 *函数功能：二分查找index对应的数值
 *返回：data[index]值
 */
int search(int start, int end, int index)
{
    int temp_index = 0;
    int mid_val = (start + end) / 2;

    while(is_containof_fourth(mid_val))
    {
        mid_val--;
    }

    temp_index = cal_fourth(mid_val);
    if(temp_index == index)
    {
        return mid_val;
    }else if(temp_index > index)
    {
        search(start, mid_val, index);
    }else{
        search(mid_val, end, index);
    }
}

/*
 *函数功能：初始查找index对应的数值
 *返回：data[index]值
 */
int find_value_by_index(int index)
{
    //这里我们所求的数肯定会在[index, 5 * index]之间
    int start = index;
    int end = index * 5;

    while(is_containof_fourth(start))
        start++;

    while(is_containof_fourth(end))
        end++;

    return search(start, end, index);
}

int main(void)
{
    int n = 0;
    int ret = 0;
    while(scanf("%d", &n) != EOF)
    {
        ret = find_value_by_index(n);
        printf("%d\n", ret);
    }

    return 0;
}
