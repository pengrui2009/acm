#include <iostream>
#include <stdlib.h>
using namespace std;

struct list{
	int data;
	struct list *next;
};

class linklist{
private:
    struct list *L;
    void reserve(struct list *l)
    {
        struct list *p, *q;

        p = l->next;

        if(p == NULL)
        {
            return;
        }

        q = p->next;
        if(q == NULL)
        {
            return;
        }

        while(q->next)
        {
            p = p->next;
            q = p->next;
        }


        p->next = NULL;

        q->next = l->next;

        l->next = q;


        reserve(q);
    }
public:
    linklist();
    ~linklist();

    bool IsEmpty();
    int Insert(int value);
    void Reserve();
    void Print();
};

linklist::linklist()
{
    L = (struct list *)malloc(sizeof(struct list));
    L->data = 0;
    L->next = NULL;
}

linklist::~linklist()
{
    struct list *node_ptr = L->next;
    struct list *temp_ptr = NULL;

    while(node_ptr)
    {
        temp_ptr = node_ptr->next;
        free(node_ptr);
        node_ptr = temp_ptr;
    }

    free(L);
}

bool linklist::IsEmpty()
{
    if(NULL == L->next)
    {
        return true;
    }

    return false;
}

int linklist::Insert(int value)
{
    int ret = 0;
    struct list *p = L->next;

    struct list *node_ptr = (struct list *)malloc(sizeof(struct list));
    if(NULL ==  node_ptr)
    {
        ret = -1;
        goto ERR_EXIT;
    }

    //insert node
    node_ptr->data = value;
    node_ptr->next = NULL;

    if(NULL == p)
    {
        L->next = node_ptr;
    }else{
        while(p->next)
        {
            p = p->next;
        }

        p->next = node_ptr;
    }

ERR_EXIT:

    return ret;
}

void linklist::Reserve()
{
    reserve(L);
}

void linklist::Print()
{
    struct list *p = L->next;

    while(p)
    {
        cout << p->data << " ";
        p = p->next;
    }

    cout << endl;
}


int main()
{
	linklist a;

	a.Insert(1);
	a.Insert(3);
	a.Insert(5);
	a.Insert(7);
    a.Print();

    a.Reserve();

    a.Print();

	return 0;
}
